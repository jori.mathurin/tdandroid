package com.example.project.Interface.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.Controller.TmdbClient;
import com.example.project.Interface.Adapter.CreditListAdapter;
import com.example.project.Interface.Adapter.FilmListAdapter;
import com.example.project.Interface.Adapter.SimilarListAdapter;
import com.example.project.Model.Cast;
import com.example.project.Model.Credits;
import com.example.project.Model.Film;
import com.example.project.Model.Genre;
import com.example.project.Model.GenreRanger;
import com.example.project.Model.GenreSearch;
import com.example.project.Model.ResultSearch;
import com.example.project.Model.ResultTranslationsRequest;
import com.example.project.Model.Translation;
import com.example.project.R;
import com.example.project.Service.DownloaderService;
import com.example.project.Service.UtilService;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailsActivity extends AppCompatActivity {
    private BottomNavigationView bottomNavigationView;
    private Film film_selected;
    private RelativeLayout modalZoomImg;
    private ScrollView scrollView;
    private TextView descriptionView, titreView, dateView, genresView, popularityView;
    private TextView titleDescription, titleCasting, titleSimilar;
    private RecyclerView recylerViewListeCredit, recyclerViewListeSimilarFilms;
    private ImageView posterLeftView, imageZoom;
    private List<Cast> casts;
    private List<Translation> translations;
    private RatingBar ratingBarVote;
    private List<Film> films_similaires;
    private String langue_actuelle = FR;
    private SharedPreferences sharedpreferences;
    private final static String SETTINGS = "settings";
    private final static String LANGUAGE = "language";
    private static final String API_KEY = "a870ad0110ff6bafa39b352d0dd503dd";
    private static final String FR = "Français", FR_API = "French";
    private static final String UK = "Anglais", UK_API = "English";
    private ProgressDialog dialog_loading;
    private List<GenreRanger> genres;
    private TmdbClient client;
    private String film_id;
    private static final int DIALOG_ALERT = 10;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        genres = new ArrayList<>();
        dialog_loading = ProgressDialog.show(DetailsActivity.this, "",
                "Chargement des données en cours...", true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedpreferences = getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        String lg = sharedpreferences.getString(LANGUAGE, "null");
        if(!lg.equals("null")) {
            langue_actuelle = lg;
        }

        //Récupération du film selectionné

        film_id = (String) getIntent().getSerializableExtra("FilmId");

        //Récupération des infos du film
        loadFilm();

        posterLeftView = findViewById(R.id.imageFilmLeft);
        posterLeftView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zoomImage();
            }
        });
        descriptionView = findViewById(R.id.descriptionFilm);
        titreView = findViewById(R.id.titreFilm);
        dateView = findViewById(R.id.releaseDateFilm);
        genresView = findViewById(R.id.genres);
        popularityView = findViewById(R.id.popularity);
        ratingBarVote = findViewById(R.id.ratingBarVote);
        recylerViewListeCredit = findViewById(R.id.RecylerViewListeCredit);
        recyclerViewListeSimilarFilms = findViewById(R.id.RecylerViewListeSimilar);
        titleDescription = findViewById(R.id.DescriptionTitle);
        titleCasting = findViewById(R.id.CastingTitle);
        titleSimilar = findViewById(R.id.SimilarTitle);
        modalZoomImg = findViewById(R.id.modalZoomImg);
        imageZoom = findViewById(R.id.imgZoom);
        scrollView = findViewById(R.id.scrollview);

        //Modification du header de la page
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Détails");
        toolbar.setSubtitle("Consulter les informations du film");
        toolbar.setLogo(android.R.drawable.ic_menu_info_details);

        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.flag_fr));

        //Configuration du menu de navigation du bas
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.activity_main_bottom_navigation);
        this.configureBottomView();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        if(langue_actuelle.equals(UK)) {
            MenuItem item;
            item = menu.findItem(R.id.action_flag);
            item.setIcon(R.drawable.flag_uk);
            item.setTitle(UK);
        }
        return true;
    }

    private void configureBottomView(){
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Intent intent = null;
                switch (menuItem.getItemId()) {
                    case R.id.action_menu_accueil:
                        intent = new Intent(DetailsActivity.this.getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_menu_search:
                        intent = new Intent(DetailsActivity.this.getApplicationContext(), SearchActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_menu_settings:
                        showDialog(DIALOG_ALERT);
                        break;
                }
                return true;
            }
        });
        bottomNavigationView.getMenu().findItem(R.id.action_menu_search).setChecked(true);
    }

    private void displayFilm() {
        titreView.setText(film_selected.getTitle());
        ratingBarVote.setRating(film_selected.getVoteCount());

        popularityView.setTextColor(UtilService.getColorPopularity(film_selected.getPopularity()));
        popularityView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DetailsActivity.this.getApplicationContext(), UtilService.getTextPopularity(popularityView.getCurrentTextColor(), langue_actuelle), Toast.LENGTH_SHORT).show();
                //Toast.makeText(DetailsActivity.this.getApplicationContext(), film_selected.getId().toString(), Toast.LENGTH_SHORT).show();
            }
        });


        if(isPage(UK)) {
            dateView.setText("Release on " + UtilService.formaterDateUK(film_selected.getReleaseDate()));
            titleDescription.setText(R.string.title_description_uk);
            titleSimilar.setText(R.string.title_similar_uk);
            titleCasting.setText(R.string.title_casting_uk);

            if(film_selected.getGenres() != null && !film_selected.getGenres().isEmpty()) {
                StringBuilder string_genre = new StringBuilder();
                for(Genre g: film_selected.getGenres()) {
                    string_genre.append(", " + g.getName());
                }
                genresView.setText(string_genre.substring(1)); //Enlever la première virgule
            }
        }
        else {
            dateView.setText("Sorti le " + UtilService.formaterDate(film_selected.getReleaseDate()));
            titleDescription.setText(R.string.title_description_fr);
            titleSimilar.setText(R.string.title_similar_fr);
            titleCasting.setText(R.string.title_casting_fr);

            if(genres != null && !genres.isEmpty()) {
                StringBuilder string_genre_fr = new StringBuilder();
                for(GenreRanger g: genres) {
                    string_genre_fr.append(", " + g.getName_fr());
                }
                genresView.setText(string_genre_fr.substring(1)); //Enlever la première virgule
            }
        }



        //Traduire le texte de description selon les settings

        //String language = sharedpreferences.getString(LANGUAGE, "Original");

        if(!langue_actuelle.equals(UK)) {
            for(Translation t : translations) {
                if(t.getEnglishName().equals(FR_API)) {
                    //set description
                    descriptionView.setText(t.getData().getOverview());
                    break;
                }
            }
        }
        else if(film_selected.getOverview() != null && !film_selected.getOverview().isEmpty()) {
            descriptionView.setText(film_selected.getOverview());
        }
        else {
            if(isPage(UK)) {
                descriptionView.setText(R.string.no_description_uk);
            }
            else {
                descriptionView.setText(R.string.no_description_fr);
            }
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog_loading.hide();
            }
        }, 500);

    }

    public void loadFilm() {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl( "https://api.themoviedb.org/3/" )
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        client = retrofit.create(TmdbClient.class);

        Call<Film> call = client.getFilmById(film_id, API_KEY);

        call.enqueue(new Callback<Film>() {
            @Override
            public void onResponse(Call<Film> call, Response<Film> response) {
                //Toast.makeText(DetailsActivity.this.getApplicationContext(), "popu : " + response.body().getPopularity(), Toast.LENGTH_SHORT).show();
                film_selected = response.body();
                for(Genre g : film_selected.getGenres()) {
                    //ranger les genres UK et genre FK
                    genres.add(new GenreRanger(g.getId(), g.getName()));
                }
                if(film_selected.getPosterPath() != null && !film_selected.getPosterPath().isEmpty()) { //Si image
                    new DownloaderService(posterLeftView.getContext(), film_selected.getPosterPath(), posterLeftView);
                }
                else { //Si pas d'image, image par défaut
                    new DownloaderService(posterLeftView.getContext(), R.drawable.image_not_found, posterLeftView);
                }
                loadCredits();
            }

            @Override
            public void onFailure(Call<Film> call, Throwable t) {
                Toast.makeText(DetailsActivity.this.getApplicationContext(), "erreur : " + t.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });
    }

    private void loadCredits() {
        Call<Credits> call_credits = client.getCreditsByFilm(film_selected.getId().toString(), API_KEY);
        call_credits.enqueue(new Callback<Credits>() {
            @Override
            public void onResponse(Call<Credits> call, Response<Credits> response) {
                casts = response.body().getCast();
                displayRecylerViewCredits();
                loadSimilarFilms();
            }

            @Override
            public void onFailure(Call<Credits> call, Throwable t) {
                Toast.makeText(DetailsActivity.this.getApplicationContext(), "erreur : " + t.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });
    }

    private void loadSimilarFilms() {
        Call<ResultSearch> call_similar = client.getSimilarFilms(film_selected.getId().toString(), API_KEY);
        call_similar.enqueue(new Callback<ResultSearch>() {
            @Override
            public void onResponse(Call<ResultSearch> call, Response<ResultSearch> response) {
                films_similaires = response.body().getListFilms();
                displayRecyclerViewSimilarFilms();
                loadTranslations();
            }

            @Override
            public void onFailure(Call<ResultSearch> call, Throwable t) {
                Toast.makeText(DetailsActivity.this.getApplicationContext(), "erreur : " + t.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });
    }

    private void loadTranslations() {
        Call<ResultTranslationsRequest> call_translations = client.getTranslationsOfFilm(film_selected.getId().toString(), API_KEY);
        call_translations.enqueue(new Callback<ResultTranslationsRequest>() {
            @Override
            public void onResponse(Call<ResultTranslationsRequest> call, Response<ResultTranslationsRequest> response) {
                translations = response.body().getTranslations();
                loadGenreFr();
            }

            @Override
            public void onFailure(Call<ResultTranslationsRequest> call, Throwable t) {
                Toast.makeText(DetailsActivity.this.getApplicationContext(), "erreur : " + t.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });
    }

    private void loadGenreFr() {
        Call<GenreSearch> call_genre_fr = client.getGenresFr(API_KEY, "fr");
        call_genre_fr.enqueue(new Callback<GenreSearch>() {
            @Override
            public void onResponse(Call<GenreSearch> call, Response<GenreSearch> response) {
                for(GenreRanger g : genres) {
                    for(Genre genre : response.body().getGenres()) {
                        if(g.getId() == genre.getId()) {
                            g.setName_fr(genre.getName());
                            break;
                        }
                    }
                }
                displayFilm();
            }

            @Override
            public void onFailure(Call<GenreSearch> call, Throwable t) {
                Toast.makeText(DetailsActivity.this.getApplicationContext(), "erreur : " + t.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });
    }

    public void displayRecylerViewCredits() {
        recylerViewListeCredit.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        recylerViewListeCredit.setAdapter(new CreditListAdapter(casts));
    }

    public void displayRecyclerViewSimilarFilms() {
        recyclerViewListeSimilarFilms.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewListeSimilarFilms.setAdapter(new SimilarListAdapter(films_similaires));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //flèche retour de la toolbar
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        //Changer la langue
        else if(item.getItemId() == R.id.action_flag) {
            SharedPreferences.Editor editor = sharedpreferences.edit();
            if(item.getTitle().equals(FR)) {
                langue_actuelle = UK;
                item.setIcon(R.drawable.flag_uk);
                item.setTitle(UK);
                editor.putString(LANGUAGE, UK);
            }
            else {
                langue_actuelle = FR;
                item.setIcon(R.drawable.flag_fr);
                item.setTitle(FR);
                editor.putString(LANGUAGE, FR);
            }
            editor.commit();
            displayFilm();

        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isPage(String langue) {
        return langue_actuelle.equals(langue);
    }

    public void zoomImage() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        new DownloaderService(imageZoom.getContext(), film_selected.getPosterPath(), imageZoom, 1200,1500);
        dialog.setView(imageZoom);
        dialog.show();
/*
        modalZoomImg.setVisibility(View.VISIBLE);
        scrollView.requestDisallowInterceptTouchEvent(true);
        modalZoomImg.bringToFront();*/

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_ALERT:
                // Create out AlterDialog
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_HOLO_DARK);
                builder.setTitle("MoviMee");
                builder.setMessage(Html.fromHtml("<p>L'application MoviMee a été dévelopée en 2019 par <b>Gauthier Vangrevelynghe</b> et <b>Jori Mathurin</b> dans le cadre d'un projet d'école.</p><p>Elle vous permet de facilement naviguer au sein d'une large collection de films populaires et d'en obtenir les détails.</p>"));
                builder.setCancelable(true);
                builder.setNeutralButton("OK", new DetailsActivity.OkOnClickListener());
                android.app.AlertDialog dialog = builder.create();
                dialog.show();
        }
        return super.onCreateDialog(id);
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
        }
    }

}
