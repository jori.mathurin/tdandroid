package com.example.project.Interface.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.Controller.TmdbClient;
import com.example.project.Interface.Adapter.PosterAdapter;
import com.example.project.Interface.Adapter.PosterAdapterGrille;
import com.example.project.Model.Film;
import com.example.project.Model.ObjectVideo;
import com.example.project.Model.ResultSearch;
import com.example.project.Model.ResultsVideo;
import com.example.project.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Grille extends AppCompatActivity {
    private final static String API_KEY="a870ad0110ff6bafa39b352d0dd503dd";
    private final static String language="FR";
    public static Context context;


    static int bound = 3;




    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.grilleactivity);
        Intent intent = getIntent();
        context = this;
        final List<String> listetitres = new ArrayList<>();
        final List<String> listeposter_url = new ArrayList<>();
        final  List<String> listevideoKeys = new ArrayList<>();
        final List<Integer> listeFilmId = new ArrayList<>();
        final int genreid =intent.getIntExtra("genreid",0);
        final String titre =intent.getStringExtra("titre");
        final String Titrefav =intent.getStringExtra("Titrefav");
        final TextView SuperTitre = findViewById(R.id.Supertitre);
        if ((genreid==0||titre==null)&& (Titrefav!=null)) {
            int i = 1;

                Retrofit.Builder builderfilmfav = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/").addConverterFactory(GsonConverterFactory.create());
                Retrofit retrofit = builderfilmfav.build();
                TmdbClient client = retrofit.create(TmdbClient.class);
                Call<ResultSearch> call;

            while (i <= 3) {

                switch (Titrefav) {
                    case "Populaire":
                        call = client.getfilmpopLanguageandPage(API_KEY, language, i);
                        SuperTitre.setText("Films Populaires");
                        break;
                    case "En ce moment":
                        call = client.getnow_playingLanguageandPage(API_KEY, language, i);
                        SuperTitre.setText("En ce moment");
                        break;
                    case "Les mieux notés":
                        call = client.gettop_ratedLanguageandPage(API_KEY, language, i);
                        SuperTitre.setText("Les mieux notés");
                        break;
                    case "Bientôt":
                        call = client.getupcomingLanguageandPage(API_KEY, language, i);
                        SuperTitre.setText("Films à venir");
                        break;
                    default:
                        call = client.getupcomingLanguageandPage(API_KEY, language, i);
                        SuperTitre.setText("Films Populaires");
                }
                call.enqueue(new Callback<ResultSearch>() {
                    @Override
                    public void onResponse(Call<ResultSearch> call,  Response<ResultSearch> response) {
                        Log.d("loogggg", "" + response.body().getResults().size());
                        assert response.body() != null;
                        for (final Film film : response.body().getResults()) {
                            final Integer FilmId = film.getId();
                            final String titre = film.getTitle();
                            final String poster_url = "https://image.tmdb.org/t/p/w300_and_h450_bestv2/" + film.getPosterPath();
                            int movie_id = film.getId();

                            Retrofit.Builder buildervideo = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/" + movie_id + "/").addConverterFactory(GsonConverterFactory.create());
                            Retrofit retrofitvideo = buildervideo.build();
                            TmdbClient ObjetVideo = retrofitvideo.create(TmdbClient.class);
                            Call<ObjectVideo> callvideo = ObjetVideo.getLanguage(API_KEY, language);
                            callvideo.enqueue(new Callback<ObjectVideo>() {
                                @Override
                                public void onResponse(Call<ObjectVideo> callvideo, Response<ObjectVideo> responsevideo) {
                                    Log.d("loogggg", "" + responsevideo.body().getResults().size());
                                    for (ResultsVideo VideoID : responsevideo.body().getResults()) {
                                        if (VideoID.getSite().equals("YouTube")) {
                                            listevideoKeys.add(VideoID.getKey());
                                            listetitres.add(titre);
                                            listeFilmId.add(FilmId);
                                            listeposter_url.add(poster_url);

                                            PosterAdapterGrille posterAdapter = new PosterAdapterGrille(listeposter_url, listevideoKeys, listetitres,listeFilmId,0);
                                            RecyclerView recyclerfilmfav = findViewById(R.id.grille);
                                            recyclerfilmfav.setLayoutManager(new GridLayoutManager(Grille.this, 3));
                                            recyclerfilmfav.setAdapter(posterAdapter);

                                            break;

                                        } else {
                                            continue;
                                        }

                                    }


                                }

                                @Override
                                public void onFailure(Call<ObjectVideo> call, Throwable t) {
                                    Toast.makeText(Grille.this, "Error...!!!", Toast.LENGTH_SHORT).show();

                                }
                            });

                        }

                    }

                    @Override
                    public void onFailure(Call<ResultSearch> call, Throwable t) {
                        Toast.makeText(Grille.this, "Error...!!!", Toast.LENGTH_SHORT).show();
                    }
                });
                i++;

            }




        }
        else if ((genreid!=0 && titre!=null)|| (Titrefav==null))
        {
            int i=1;
            SuperTitre.setText(titre);
            Retrofit.Builder buildergenre = new Retrofit.Builder().baseUrl("https:///api.themoviedb.org/3/discover/").addConverterFactory(GsonConverterFactory.create());
            Retrofit retrofitgenre = buildergenre.build();
            TmdbClient ObjetfilmsGenre = retrofitgenre.create(TmdbClient.class);
            while (i<=3) {

                Call<ResultSearch> Callgenre = ObjetfilmsGenre.getfilmsgenresLanguageandPage(API_KEY,genreid,language,i);
                Callgenre.enqueue(new Callback<ResultSearch>() {
                    @Override
                    public void onResponse(Call<ResultSearch> Callgenre, Response<ResultSearch> Responsegenre) {

                        assert Responsegenre.body() != null;
                        for (Film film : Responsegenre.body().getResults()) {
                            final String titre = film.getTitle();
                            final String poster_url = "https://image.tmdb.org/t/p/w300_and_h450_bestv2/" + film.getPosterPath();
                            final int movie_id = film.getId();
                            Retrofit.Builder buildervideo = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/" + movie_id + "/").addConverterFactory(GsonConverterFactory.create());
                            Retrofit retrofitvideo = buildervideo.build();
                            TmdbClient ObjetVideo = retrofitvideo.create(TmdbClient.class);
                            Call<ObjectVideo> callvideo = ObjetVideo.getLanguage(API_KEY, language);
                            callvideo.enqueue(new Callback<ObjectVideo>() {
                                @Override
                                public void onResponse(Call<ObjectVideo> callvideo, Response<ObjectVideo> responsevideo) {

                                    for (ResultsVideo VideoID : responsevideo.body().getResults()) {
                                        if (VideoID.getSite().equals("YouTube")) {
                                            listevideoKeys.add(VideoID.getKey());
                                            listetitres.add(titre);
                                            listeFilmId.add(movie_id);
                                            listeposter_url.add(poster_url);

                                            PosterAdapterGrille posterAdapter = new PosterAdapterGrille(listeposter_url,listevideoKeys,listetitres,listeFilmId,0);
                                            RecyclerView recyclerfilmfav = findViewById(R.id.grille);
                                            recyclerfilmfav.setLayoutManager(new GridLayoutManager(Grille.this, 3));
                                            recyclerfilmfav.setAdapter(posterAdapter);

                                            break;
                                        } else {
                                            continue;
                                        }
                                    }

                                }

                                @Override
                                public void onFailure(Call<ObjectVideo> call, Throwable t)
                                {
                                    Toast.makeText(Grille.this, "Error...!!!", Toast.LENGTH_SHORT).show();

                                }
                            });
                        }


                    }

                    @Override
                    public void onFailure(Call<ResultSearch> call, Throwable t)
                    {
                        Toast.makeText(Grille.this, "Error...!!!", Toast.LENGTH_SHORT).show();

                    }

                });

              i++;
            }
        }

    }
    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }
}
