package com.example.project.Interface.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.project.Interface.Activity.MainActivity;
import com.example.project.Model.Genre;
import com.example.project.Model.ObjectVideo;
import com.example.project.Model.ResultSearch;
import com.example.project.Model.ResultsVideo;
import com.example.project.R;
import com.example.project.Service.RequeteMaker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.GenreViewHolder> {
    private List<Genre> genres;
    private List<String> Titre = new ArrayList<>();
    private final RequeteMaker requete = new RequeteMaker();



    /**
     * @param genres
     */
    public GenreAdapter(List<Genre> genres) {
        this.genres = genres;
        for (Genre var : this.genres) {
            Titre.add(var.getName());

        }

    }



    @Override
    public GenreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MainActivity.spinner.setVisibility(View.VISIBLE);

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerlayout, parent, false);
        return new GenreViewHolder(view);


    }

    @Override
    public void onBindViewHolder(GenreViewHolder holder, int position) {

            List<String> Url = new ArrayList<>();
            List<String> Titres = new ArrayList<>();
            List<Integer> FilmId = new ArrayList<>();
            final List<String> videoUrl = new ArrayList<>();
            holder.itemView.setTag(position);
            TextView Tv = holder.getTitre();
            String titre = Titre.get(position);
            Tv.setText(titre);
            Integer titreId = 1;

                    titreId = genres.get(position).getId();
                        ResultSearch filmsgenre = null;
                        try {
                            filmsgenre = requete.getfilmsbygenre("FR", 1, titreId);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        assert filmsgenre != null;

                        int rand = MainActivity.getRandomNumberInRange(4, 5);
                        for (int i = 0; i < rand; i++) {


                            ObjectVideo objectVideo = null;
                            try {
                                objectVideo = requete.gevideoUrl("FR", filmsgenre.getResults().get(i).getId());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            assert objectVideo != null;
                            for (ResultsVideo VideoID : objectVideo.getResults()) {
                                if (VideoID.getSite().equals("YouTube") && !Titres.contains(filmsgenre.getResults().get(i).getTitle())) {
                                    videoUrl.add(VideoID.getKey());
                                    String poster_url = "https://image.tmdb.org/t/p/w300_and_h450_bestv2/" + filmsgenre.getResults().get(i).getPosterPath();
                                    String filmtitre = filmsgenre.getResults().get(i).getTitle();
                                    Titres.add(filmtitre);
                                    Url.add(poster_url);
                                    FilmId.add(filmsgenre.getResults().get(i).getId());

                                    break;
                                }

                            }

                    }


        RecyclerView recycler = holder.getRecyclerFilm();
        recycler.setLayoutManager(new LinearLayoutManager(recycler.getContext(), LinearLayoutManager.HORIZONTAL, false));
        recycler.setAdapter(new PosterAdapter(Url, videoUrl,Titres,FilmId,0));
        MainActivity.spinner.setVisibility(View.GONE);
    }



    @Override
    public int getItemCount() { return genres.size(); }

    class GenreViewHolder extends RecyclerView.ViewHolder
    {
        private TextView GenreTitre;
        private RecyclerView recyclerFilm;

        GenreViewHolder(View itemView)


        {
            super(itemView);
            GenreTitre=itemView.findViewById(R.id.Tireder);
            recyclerFilm=itemView.findViewById(R.id.recyclerFilm);

        }
        TextView  getTitre(){return this.GenreTitre;}
        RecyclerView getRecyclerFilm(){return this.recyclerFilm;}
    }
}
