package com.example.project.Model;

public class GenreRanger {

    private int id;
    private String name_uk;
    private String name_fr;

    public GenreRanger(int id, String name_uk) {
        this.id = id;
        this.name_uk = name_uk;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName_uk() {
        return name_uk;
    }

    public void setName_uk(String name_uk) {
        this.name_uk = name_uk;
    }

    public String getName_fr() {
        return name_fr;
    }

    public void setName_fr(String name_fr) {
        this.name_fr = name_fr;
    }
}
