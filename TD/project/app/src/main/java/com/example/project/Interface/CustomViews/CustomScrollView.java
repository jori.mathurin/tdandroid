package com.example.project.Interface.CustomViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class CustomScrollView extends ScrollView {
    private boolean is_scroll_enabled = true;

    public CustomScrollView(Context context) {
        super(context);
    }

    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setScrollEnabled(boolean bool) {
        this.is_scroll_enabled = bool;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if(!is_scroll_enabled) {
            return false;
        }
        else{
            super.onInterceptTouchEvent(ev);
        }
        return true;
    }

}
