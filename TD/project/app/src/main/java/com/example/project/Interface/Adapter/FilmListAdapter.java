package com.example.project.Interface.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import com.example.project.Controller.TmdbClient;
import com.example.project.Interface.ViewHolder.FilmListViewHolder;
import com.example.project.Model.Film;
import com.example.project.Model.ResultTranslationsRequest;
import com.example.project.Model.Translation;
import com.example.project.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FilmListAdapter extends RecyclerView.Adapter<FilmListViewHolder>{

    private List<Film> films;
    private String language;
    private Film current_film;
    private static final String API_KEY = "a870ad0110ff6bafa39b352d0dd503dd";
    private String trueDescription= "";
    private FilmListViewHolder viewHolder;

    public FilmListAdapter(List<Film> listeToDisplay, String language) {this.films = listeToDisplay; this.language = language;}

    @Override
    public FilmListViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_film_search, parent, false);
        return new FilmListViewHolder(view, films);
    }

    @Override
    public void onBindViewHolder(FilmListViewHolder holder, int position) {
        current_film = films.get(position);
        viewHolder = holder;
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl( "https://api.themoviedb.org/3/" )
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        TmdbClient client = retrofit.create(TmdbClient.class);

        /*Call<ResultTranslationsRequest> call = client.getTranslationsOfFilm(current_film.getId().toString(),API_KEY);

        call.enqueue(new Callback<ResultTranslationsRequest>() {
            @Override
            public void onResponse(Call<ResultTranslationsRequest> call, Response<ResultTranslationsRequest> response) {
                if(!language.equals("Original")) {
                    for(Translation t : response.body().getTranslations()) {
                        if (t.getEnglishName().equals(language)) {
                            //set description
                            trueDescription = t.getData().getOverview();
                            break;
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<ResultTranslationsRequest> call, Throwable t) {
            }

        });*/

        viewHolder.display(current_film);

    }

    @Override
    public int getItemCount() {
        return films.size();
    }
}