package com.example.project.Model;

import android.content.Intent;

import android.os.Bundle;
import android.util.Log;

import android.widget.Toast;

import com.example.project.Interface.Activity.MainActivity;
import com.example.project.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;


public class VideoPlayer extends YouTubeBaseActivity {

    private MyPlayerStateChangeListener playerStateChangeListener;
    private MyPlaybackEventListener playbackEventListener;
    //Clé de l'application
    private final String APIKEY ="AIzaSyCo5a8q3745Ek0kZpkpx_06Ca2Hquk8BB0";
    YouTubePlayerView Player;


    //Notification
    private void showMessage(String message)
    {
        if(message.equals("End")) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);

        }
    }


    //Convertir l'url en VIDEOCODE



    @Override
    protected void onCreate(Bundle savedInstanceState)

    {
        //Event Listener
        playerStateChangeListener = new MyPlayerStateChangeListener();
        playbackEventListener = new MyPlaybackEventListener();

        //Initisialisation
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        Player = (YouTubePlayerView)findViewById(R.id.YoutubePlayer);


        //Récupération de l'url
        Intent intent = getIntent();
        final String url =intent.getStringExtra("url");

        Log.d("*************", "****CorrectURL*****"+url+"************");


        //Initialisation du lecteur

        Player.initialize(APIKEY, new YouTubePlayer.OnInitializedListener()
                {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b)

            {
                youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
                youTubePlayer.setPlaybackEventListener(playbackEventListener);
               if(!b)
               {
                   youTubePlayer.loadVideo(url);
                   youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);



               }
            }


            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult)
            {
                Toast.makeText(VideoPlayer.this,youTubeInitializationResult.toString(),
                        Toast.LENGTH_SHORT).show();
                Log.d("*************", "*********"+youTubeInitializationResult.toString()+"************");

            }


        }


        );

    }


    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {

        @Override
        public void onPlaying()
        {
            // Called when playback starts, either due to user action or call to play().
            showMessage("Playing");
        }

        @Override
        public void onPaused()
        {
            // Called when playback is paused, either due to user action or call to pause().
            showMessage("Paused");
        }

        @Override
        public void onStopped()
        {
            // Called when playback stops for a reason other than being paused.

            showMessage("Stopped");
        }

        @Override
        public void onBuffering(boolean b)
        {
            // Called when buffering starts or ends.
        }

        @Override
        public void onSeekTo(int i)

        {
            // Called when a jump in playback position occurs, either
            // due to user scrubbing or call to seekRelativeMillis() or seekToMillis()
        }


    }

    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener
    {

        @Override
        public void onLoading()
        {
            // Called when the player is loading a video
            // At this point, it's not ready to accept commands affecting playback such as play() or pause()
        }

        @Override
        public void onLoaded(String s)
        {
            // Called when a video is done loading.
            // Playback methods such as play(), pause() or seekToMillis(int) may be called after this callback.
        }

        @Override
        public void onAdStarted()
        {
            // Called when playback of an advertisement starts.
        }

        @Override
        public void onVideoStarted()
        {
            // Called when playback of the video starts.
        }

        @Override
        public void onVideoEnded()
        {

            showMessage("End");



        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason)

        {

        }


    }





}
