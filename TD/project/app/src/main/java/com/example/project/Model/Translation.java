package com.example.project.Model;

public class Translation {

    private String iso_3166_1;
    private String iso_639_1;
    private String name;
    private String english_name;
    private TranslationsData data;

    public String getIso31661() {
        return iso_3166_1;
    }

    public void setIso31661(String iso31661) {
        this.iso_3166_1 = iso31661;
    }

    public String getIso6391() {
        return iso_639_1;
    }

    public void setIso6391(String iso6391) {
        this.iso_639_1 = iso6391;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnglishName() {
        return english_name;
    }

    public void setEnglishName(String englishName) {
        this.english_name = englishName;
    }

    public TranslationsData getData() {
        return data;
    }

    public void setData(TranslationsData data) {
        this.data = data;
    }

}