package com.example.project.Model;

import java.util.List;

public class ResultTranslationsRequest {
    private int id;
    private List<Translation> translations;

    public ResultTranslationsRequest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Translation> getTranslations() {
        return translations;
    }

    public void setTranslations(List<Translation> translations) {
        this.translations = translations;
    }
}
