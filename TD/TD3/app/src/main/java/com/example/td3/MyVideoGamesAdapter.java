package com.example.td3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyVideoGamesAdapter extends RecyclerView.Adapter<MyVideoGamesViewHolder>



{


    private List<JeuVideo> mesJeux;

    public MyVideoGamesAdapter(List<JeuVideo> mesJeux) {
        this.mesJeux = mesJeux;
    }
    @NonNull
    @Override
    public MyVideoGamesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.jeuvideo,
                parent, false);
        return new MyVideoGamesViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull MyVideoGamesViewHolder holder, int position)
    {
        holder.display(mesJeux.get(position));
    }
    @Override
    public int getItemCount() {
        return mesJeux.size();
    }
}

class MyVideoGamesViewHolder extends RecyclerView.ViewHolder {
    private TextView mNameTV;
    private TextView mPriceTV;
    public MyVideoGamesViewHolder(@NonNull View itemView) {
        super(itemView);
        mNameTV = itemView.findViewById(R.id.name);
        mPriceTV = itemView.findViewById(R.id.price);
    }
    void display(JeuVideo jeuVideo){
        mNameTV.setText(jeuVideo.getName());
        mPriceTV.setText(jeuVideo.getPrice() + "$");
    }
}

