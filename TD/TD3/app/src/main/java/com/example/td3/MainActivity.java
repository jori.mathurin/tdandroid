package com.example.td3;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity  extends AppCompatActivity
{





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //CREER ICI UNE LISTE DE JEUX VIDEO NOMMEE mesJeux ET REMPLISSEZ LA DE JeuxVideo
        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<JeuVideo>mesJeux=new ArrayList<JeuVideo>();

        mesJeux.add(new JeuVideo("Soul CALIBUR",10));
        mesJeux.add(new JeuVideo("BIOSHOCK",22));
        mesJeux.add(new JeuVideo("TEKKEN",30));
        mesJeux.add(new JeuVideo("Red Dead Redemption",100000));


        myRecyclerView.setAdapter( new MyVideoGamesAdapter(mesJeux));
    }



}
