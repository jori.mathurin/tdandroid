package com.example.td2;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class NewsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        List<String>list2= new ArrayList<>();
        for (int i=0 ; i< 20 ; i++)
        {
            list2.add("Article"+i);

        }


        StringBuilder builder = new StringBuilder();
        for (String details : list2) {
            builder.append(details + "\n");
        }
        TextView liste=findViewById(R.id.liste);

        liste.setText(builder.toString());


    }

    @Override
    public void onBackPressed () {

        moveTaskToBack(true);
    }

    public void godetail(View v) {

        Intent intent;
        intent = new Intent(this, DetailsActivity.class);
        startActivity(intent);
    }
}
