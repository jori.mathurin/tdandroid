package com.example.td4;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
public class MyIDAdapter extends RecyclerView.Adapter<IdViewHolder> {

    private List<VideoId> IdLIist;

    MyIDAdapter(List<VideoId>IdList)
    {

        this.IdLIist=IdList;
    }


    @NonNull
    @Override
    public IdViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view,
                parent, false);
        return new IdViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IdViewHolder holder, int position)
    {
        holder.display(IdLIist.get(position));


    }

    @Override
    public int getItemCount()
    {
        return 0;
    }
}

class IdViewHolder extends RecyclerView.ViewHolder {
    private TextView Repo;


    public IdViewHolder(@NonNull View itemView) {
        super(itemView);
        Repo = itemView.findViewById(R.id.name);


    }

    void display(VideoId id){
        Repo.setText(id.getVideoID());

    }
}
