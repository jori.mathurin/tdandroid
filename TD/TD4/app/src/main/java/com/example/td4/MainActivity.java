package com.example.td4;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("https://www.googleapis.com").addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        YoutubeClient client = retrofit.create(YoutubeClient.class);
        Call<List<VideoId>> call = client.UserRepositories("PLyAp3wRCuVOHjhhiJX_8O6v9S2VLUrlAn");
        final RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        call.enqueue(new Callback<List<VideoId>>()
        {
            @Override
            public void onResponse(Call<List<VideoId>> call, Response<List<VideoId>> response)
            {


                List<VideoId> list = response.body();
                Log.d("feveververv", list.toString());

                myRecyclerView.setAdapter( new MyIDAdapter(list));

            }

            @Override
            public void onFailure(Call<List<VideoId>> call, Throwable t) {

                Toast.makeText(MainActivity.this, "Error...!!!", Toast.LENGTH_SHORT).show();
            }
        });

    }



}
