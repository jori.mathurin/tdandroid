package com.example.td4;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface YoutubeClient
{
    @GET("youtube/v3/playlistItems?part=id&playlistId={PlaylistID}&key=AIzaSyCLmuBcPfsDRSir4aChzod9VJa9NsNJyvw HTTP/1.1")
    Call<List<VideoId>> UserRepositories(@Path("Playlistid") String PlaylistID) ;
}
